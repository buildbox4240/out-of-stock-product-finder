(ns scraper.protocols.site
  (:gen-class))

(require '[net.cgrand.enlive-html :as html])

(defprotocol Site
  (in-stock? [x] "Determines whether the item is sold out on the site"))

(defrecord BestBuy [url parsed-page]
  Site
  (in-stock? [x]
    (nil? (get-in (nth (html/select parsed-page [:div.fulfillment-add-to-cart-button :button]) 0) [:attrs :disabled]))))

(defrecord NewEgg [url parsed-page]
  Site
  (in-stock? [x]
    (empty? (html/select parsed-page [:div.product-buy :span.btn-message]))))
