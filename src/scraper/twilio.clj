(ns scraper.twilio
  (:gen-class))

(require '[clj-http.client :as client])

(def message-endpoint "https://api.twilio.com/2010-04-01")

(defn get-sms-endpoint
  [sid]
  (str message-endpoint "/Accounts/" sid "/Messages.json" ))

(defn send-text
  [{sid :sid token :token} {to :to from :from body :body}]
  (client/post (get-sms-endpoint sid) {:form-params {:To to :From from :Body body} :basic-auth [sid token]}))
