(ns scraper.core
  (:gen-class))

(require '[clojure.java.io :as io])
(require '[net.cgrand.enlive-html :as html])
(require '[scraper.twilio :as twilio])
(require '[scraper.protocols.site])
(import '[scraper.protocols.site BestBuy])
(import '[scraper.protocols.site NewEgg])
(require '[clojure.core.reducers :as r])
(require '[config.core :refer [env]])
(require '[clojure.string :as str])

(defn fetch-url
  [url]
  (html/html-resource (java.net.URL. url)))

(def urls
  (pmap (fn [url]
    (cond
      (str/includes? url "bestbuy") (BestBuy. url (fetch-url url))
      (str/includes? url "newegg") (NewEgg. url (fetch-url url))))
  (str/split (slurp (io/resource "urls.txt")) #"\n")))

(def config
  {:sid (:sid env) :token (:token env)})

(def message-config
  {:from (:from env)})

(defn notify
  [message]
  (doseq [to (:to env)]
    (twilio/send-text config (assoc message-config :body message :to to))))

(defn -main
  [& args]
  (doseq [current-page urls]
    (if (.in-stock? current-page)
      (notify (:url current-page)))))
