(defproject scraper "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"], [enlive "1.1.6"], [clj-http "3.10.3"], [yogthos/config "1.1.7"]]
  :resource-paths ["resources"]
  :jvm-opts ["-Dconfig=config.edn"]
  :main ^:skip-aot scraper.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
